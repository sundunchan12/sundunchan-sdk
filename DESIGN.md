# Design of the lord of the rings sdk

There is main class named LordOfTheRingsSdk. That class contains fields that are implementations of the interfaces of each service. 
There are interfaces which contains declaration of methods 
that the user can work with. 
The services based on these interfaces are implemented with some logic.
One http client and a logger are injected into these services as needed. 