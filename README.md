# sundunchan-the-lord-of-the-rings-sdk

Introducing the lord of the rings SDK that works with https://the-one-api.dev/ API. 
Was written in C#

## Getting started
All you need to make an nuget package with an SDK installed is run the following command
```
nuget install SunDunChan.TheLordOfTheRings.Sdk
```
## SDK Initialization
For using SDK you are need API key that you can get from https://the-one-api.dev/login

You have to install nuget package to your application and create SDK class instance using 
following parameters:

apiKey: string (key that was retrieved from API)

useLogger: bool (set true if you want see logs in your terminal)

```
using TheLordOfTheRings.Sdk;

var mySdk = new LordOfTheRingsSdk("yourApiKey");
```
or if you want use useLogger

```
using TheLordOfTheRings.Sdk;

var mySdk = new LordOfTheRingsSdk("yourApiKey", true);
```

# Usage
Usage
The SDK models the One API and each property from the SDK matches 
a section in the API documentation(https://the-one-api.dev/documentation)

For example get all books:
```
var books = await mySdk.Books.GetAllAsync();
```
or get character by name:
```
var character = await mySdk.Characters.GetByNameAsync("characterName");
``` 
# List of methods
## Books
```
    Task<IEnumerable<Book>> GetAllAsync();

    Task<Book> GetAsync(string id);

    Task<Book> GetByNameAsync(string name);

    Task<IEnumerable<Chapter>> GetChaptersAsync(string bookId);

    Task<IEnumerable<Chapter>> GetChaptersAsync(Book book);

    Task<IEnumerable<string>> GetChapterTitlesAsync(string bookId);

    Task<IEnumerable<string>> GetChapterTitlesAsync(Book book);
```
## Chapters
```
    Task<IEnumerable<Chapter>> GetAllAsync();

    Task<Chapter> GetAsync(string id);

    Task<Chapter> GetByNameAsync(string name);

    Task<IEnumerable<Chapter>> GetAllByBookAsync(string bookId);
    
    Task<IEnumerable<Chapter>> GetAllByBookAsync(Book book);
```
## Characters
```
    Task<IEnumerable<Character>> GetAllAsync();
        
    Task<Character> GetAsync(string id);

    Task<IEnumerable<Character>> GetByNameAsync(string name);

    Task<IEnumerable<Character>> GetByRaceAsync(string race);

    Task<IEnumerable<Character>> GetByRacesAsync(params string[] races);

    Task<IEnumerable<Character>> GetCharactersWithSpouseAsync();

    Task<IEnumerable<Character>> GetCharactersWithoutSpouseAsync();

    Task<IEnumerable<Character>> GetMaleCharactersAsync();

    Task<IEnumerable<Character>> GetFemaleCharactersAsync();

    Task<IEnumerable<string>> GetRacesAsync();

    Task<IEnumerable<Quote>> GetQuotesAsync(string characterId);

    Task<IEnumerable<Quote>> GetQuotesAsync(Character character);

```
## Movies
```
    Task<IEnumerable<Movie>> GetAllAsync();

    Task<Movie> GetAsync(string id);

    Task<Movie> GetByNameAsync(string name);

    Task<IEnumerable<Quote>> GetQuotesAsync(string movieId);

    Task<IEnumerable<Quote>> GetQuotesAsync(Movie movie);
```
## Quotes
```
    Task<IEnumerable<Quote>> GetAllAsync();

    Task<Quote> GetAsync(string id);

    Task<IEnumerable<Quote>> GetByDialogAsync(string dialog);

    Task<IEnumerable<Quote>> GetByCharacterAsync(string characterId);

    Task<IEnumerable<Quote>> GetByCharacterAsync(Character character);
    
    Task<IEnumerable<Quote>> GetByMovieAsync(string movieId);

    Task<IEnumerable<Quote>> GetByMovieAsync(Movie movie);
```