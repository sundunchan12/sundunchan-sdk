﻿using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Interfaces;

public interface ICharacterService
{
    Task<IEnumerable<Character>> GetAllAsync();
    
    Task<Character> GetAsync(string id);
    
    Task<IEnumerable<Character>> GetByNameAsync(string name);

    Task<IEnumerable<Character>> GetByRaceAsync(string race);

    Task<IEnumerable<Character>> GetByRacesAsync(params string[] races);
    Task<IEnumerable<Character>> GetCharactersWithSpouseAsync();
    
    Task<IEnumerable<Character>> GetCharactersWithoutSpouseAsync();
    
    Task<IEnumerable<Character>> GetMaleCharactersAsync();
    
    Task<IEnumerable<Character>> GetFemaleCharactersAsync();
    
    Task<IEnumerable<string>> GetRacesAsync();
    
    
    Task<IEnumerable<Quote>> GetQuotesAsync(string characterId);
    
    Task<IEnumerable<Quote>> GetQuotesAsync(Character character);

}