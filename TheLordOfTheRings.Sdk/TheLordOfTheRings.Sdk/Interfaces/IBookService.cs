﻿using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Interfaces;

public interface IBookService
{
    Task<IEnumerable<Book>> GetAllAsync();
    Task<Book> GetAsync(string id);
    Task<Book> GetByNameAsync(string name);
    Task<IEnumerable<Chapter>> GetChaptersAsync(string bookId);
    Task<IEnumerable<Chapter>> GetChaptersAsync(Book book);
    Task<IEnumerable<string>> GetChapterTitlesAsync(string bookId);
    Task<IEnumerable<string>> GetChapterTitlesAsync(Book book);
}