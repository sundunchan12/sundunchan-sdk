﻿
using TheLordOfTheRings.Sdk.Models;
using TheLordOfTheRings.Sdk.Services;

namespace TheLordOfTheRings.Sdk.Interfaces;

public interface IChapterService
{
    Task<IEnumerable<Chapter>> GetAllAsync();
    Task<Chapter> GetAsync(string id);
    Task<Chapter> GetByNameAsync(string name);
    Task<IEnumerable<Chapter>> GetAllByBookAsync(string bookId);
    Task<IEnumerable<Chapter>> GetAllByBookAsync(Book book);
}