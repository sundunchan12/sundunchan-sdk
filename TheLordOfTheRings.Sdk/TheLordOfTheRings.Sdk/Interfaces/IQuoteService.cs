﻿using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Interfaces;

public interface IQuoteService
{
    Task<IEnumerable<Quote>> GetAllAsync();

    Task<Quote> GetAsync(string id);

    Task<IEnumerable<Quote>> GetByDialogAsync(string dialog);

    Task<IEnumerable<Quote>> GetByCharacterAsync(string characterId);

    Task<IEnumerable<Quote>> GetByCharacterAsync(Character character);
    
    Task<IEnumerable<Quote>> GetByMovieAsync(string movieId);

    Task<IEnumerable<Quote>> GetByMovieAsync(Movie movie);
}