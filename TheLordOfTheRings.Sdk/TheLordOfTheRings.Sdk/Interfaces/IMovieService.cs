﻿using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Interfaces;

public interface IMovieService
{
    Task<IEnumerable<Movie>> GetAllAsync();

    Task<Movie> GetAsync(string id);

    Task<Movie> GetByNameAsync(string name);

    Task<IEnumerable<Quote>> GetQuotesAsync(string movieId);

    Task<IEnumerable<Quote>> GetQuotesAsync(Movie movie);
}