﻿using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using TheLordOfTheRings.Contracts;
using TheLordOfTheRings.Contracts.Responses;
using TheLordOfTheRings.Sdk.Interfaces;
using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Services;

public class MovieService : IMovieService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger _logger;
    
    public MovieService(HttpClient httpClient, ILogger logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<IEnumerable<Movie>> GetAllAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Movies.GetAll}");
            var apiResponse =  await _httpClient.GetFromJsonAsync<ApiResponse<Movie>>(ApiRoutes.Movies.GetAll);
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Movie> GetAsync(string id)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Movies.Get.Replace("{id}", id)}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Movie>>(ApiRoutes.Movies.Get.Replace("{id}", id));
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no movie by this id."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Movie> GetByNameAsync(string name)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Movies.GetAll + $"?name={name}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Movie>>(ApiRoutes.Movies.GetAll + $"?name={name}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no movies by this name."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetQuotesAsync(string movieId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.GetAll + $"?movie={movieId}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.GetAll + $"?movie={movieId}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no quotes by this movie or movie not found."); 
            return apiResponse.Data.ToArray();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetQuotesAsync(Movie movie)
    {
        return await GetQuotesAsync(movie.Id);
    }
}