﻿using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using TheLordOfTheRings.Contracts;
using TheLordOfTheRings.Contracts.Responses;
using TheLordOfTheRings.Sdk.Interfaces;
using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Services;

public class BookService : IBookService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger _logger;
    
    public BookService(HttpClient httpClient, ILogger logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }
    
    public async Task<IEnumerable<Book>> GetAllAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Books.GetAll}");
            var apiResponse =  await _httpClient.GetFromJsonAsync<ApiResponse<Book>>(ApiRoutes.Books.GetAll);
            
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Book> GetAsync(string id)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Books.Get.Replace("{id}", id)}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Book>>(ApiRoutes.Books.Get.Replace("{id}", id));
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no book by this id."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Book> GetByNameAsync(string name)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Books.GetAll + $"?name={name}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Book>>(ApiRoutes.Books.GetAll + $"?name={name}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no book by this name."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
        
    }

    public async Task<IEnumerable<Chapter>> GetChaptersAsync(string bookId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Chapters.GetAll + $"?book={bookId}"} ");
            var apiResponse =
                await _httpClient.GetFromJsonAsync<ApiResponse<Chapter>>(ApiRoutes.Chapters.GetAll + $"?book={bookId}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no chapters in this book or book not found.");
            return apiResponse.Data;
        }
        catch (ArgumentException e)
        {
            _logger.LogError(e.Message);
            throw;
        }
        catch (StackOverflowException e)
        {
            _logger.LogError(e.Message);
            throw;
        }
        
    }

    public async Task<IEnumerable<Chapter>> GetChaptersAsync(Book book)
    {
        return await GetChaptersAsync(book.Id);
    }

    public async Task<IEnumerable<string>> GetChapterTitlesAsync(string bookId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Chapters.GetAll + $"?book={bookId}"} ");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Chapter>>(ApiRoutes.Chapters.GetAll + $"?book={bookId}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no chapters in this book or book not found.");
            var chaptersArray = apiResponse.Data.ToArray();
            var chapterTitles = new List<string>();
            
            for(int i = 0; i < chaptersArray.Length; i++) chapterTitles.Add(chaptersArray[i].Name);
            
            return chapterTitles;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<string>> GetChapterTitlesAsync(Book book)
    {
        return await GetChapterTitlesAsync(book.Id);
    }
}