﻿using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using TheLordOfTheRings.Contracts;
using TheLordOfTheRings.Contracts.Responses;
using TheLordOfTheRings.Sdk.Interfaces;
using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Services;

public class ChapterService : IChapterService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger _logger;
    
    public ChapterService(HttpClient httpClient, ILogger logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<IEnumerable<Chapter>> GetAllAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Chapters.GetAll}");
            var apiResponse =  await _httpClient.GetFromJsonAsync<ApiResponse<Chapter>>(ApiRoutes.Chapters.GetAll);
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Chapter> GetAsync(string id)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Chapters.Get.Replace("{id}", id)}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Chapter>>(ApiRoutes.Chapters.Get.Replace("{id}", id));
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no chapter by this id."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Chapter> GetByNameAsync(string name)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Chapters.GetAll + $"?name={name}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Chapter>>(ApiRoutes.Chapters.GetAll + $"?name={name}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no chapter by this name."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Chapter>> GetAllByBookAsync(string bookId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Chapters.GetAll + $"?book={bookId}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Chapter>>(ApiRoutes.Chapters.GetAll + $"?book={bookId}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no chapters by this book id or book not found."); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Chapter>> GetAllByBookAsync(Book book)
    {
        return await GetAllByBookAsync(book.Id);
    }
}