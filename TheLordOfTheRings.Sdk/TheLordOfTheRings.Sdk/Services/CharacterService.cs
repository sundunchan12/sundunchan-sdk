﻿using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using TheLordOfTheRings.Contracts;
using TheLordOfTheRings.Contracts.Responses;
using TheLordOfTheRings.Sdk.Interfaces;
using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Services;

public class CharacterService : ICharacterService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger _logger;
    
    public CharacterService(HttpClient httpClient, ILogger logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<IEnumerable<Character>> GetAllAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll}");
            var apiResponse =  await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll);
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Character> GetAsync(string id)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.Get.Replace("{id}", id)}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.Get.Replace("{id}", id));
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no character by this id."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Character>> GetByNameAsync(string name)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll + $"?name={name}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll + $"?name={name}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no characters by this name."); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Character>> GetByRaceAsync(string race)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll + $"?race={race}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll + $"?race={race}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no characters by this race."); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }
    
    public async Task<IEnumerable<Character>> GetByRacesAsync(params string[] races)
    {
        try
        {
            string racesQuery = string.Empty;
            for (int i = 0; i < races.Length; i++) racesQuery += races[i] + ",";
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll + $"?race={racesQuery}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll + $"?race={racesQuery}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no character by this race."); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Character>> GetCharactersWithSpouseAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll);
            apiResponse.Data = apiResponse.Data.Where(c => c.Spouse != "");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no character with spouse"); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Character>> GetCharactersWithoutSpouseAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll);
            apiResponse.Data = apiResponse.Data.Where(c => c.Spouse == "");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no character with spouse"); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Character>> GetMaleCharactersAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll + $"?gender=Male"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll + $"?gender=Male");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no characters with this gender"); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Character>> GetFemaleCharactersAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll + $"?gender=Female"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll + $"?gender=Female");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no characters with this gender"); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<string>> GetRacesAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Characters.GetAll}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Character>>(ApiRoutes.Characters.GetAll);
            var races = apiResponse.Data.Select(c => c.Race )
                .Distinct();
            
            return races;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetQuotesAsync(string characterId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.GetAll + $"?character={characterId}"} ");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.GetAll + $"?character={characterId}");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no quotes by this character or character not found."); 
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetQuotesAsync(Character character)
    {
        return await GetQuotesAsync(character.Id);
    }
    
}