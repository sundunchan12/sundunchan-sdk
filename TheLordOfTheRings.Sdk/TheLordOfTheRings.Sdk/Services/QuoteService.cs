﻿using System.Net.Http.Json;
using Microsoft.Extensions.Logging;
using TheLordOfTheRings.Contracts;
using TheLordOfTheRings.Contracts.Responses;
using TheLordOfTheRings.Sdk.Interfaces;
using TheLordOfTheRings.Sdk.Models;

namespace TheLordOfTheRings.Sdk.Services;

public class QuoteService : IQuoteService
{
    private readonly HttpClient _httpClient;
    private readonly ILogger _logger;
    
    public QuoteService(HttpClient httpClient, ILogger logger)
    {
        _httpClient = httpClient;
        _logger = logger;
    }

    public async Task<IEnumerable<Quote>> GetAllAsync()
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.GetAll}");
            var apiResponse =  await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.GetAll+"?limit=100000");
            return apiResponse.Data;
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<Quote> GetAsync(string id)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.Get.Replace("{id}", id)}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.Get.Replace("{id}", id));
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no quote by this id."); 
            return apiResponse.Data.ToArray()[0];
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetByDialogAsync(string dialog)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.GetAll + $"?dialog={dialog}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.GetAll + $"?dialog={dialog}" + "&limit=100000");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no quotes by this dialog."); 
            return apiResponse.Data.ToArray();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetByCharacterAsync(string characterId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.GetAll + $"?character={characterId}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.GetAll + $"?character={characterId}"+"&limit=100000");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no quotes by this character or character not found."); 
            return apiResponse.Data.ToArray();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetByCharacterAsync(Character character)
    {
        return await GetByCharacterAsync(character.Id);
    }
    
    public async Task<IEnumerable<Quote>> GetByMovieAsync(string movieId)
    {
        try
        {
            _logger.LogInformation($"Sending get request to {ApiRoutes.Quotes.GetAll + $"?movie={movieId}"}");
            var apiResponse = await _httpClient.GetFromJsonAsync<ApiResponse<Quote>>(ApiRoutes.Quotes.GetAll + $"?movie={movieId}"+"&limit=100000");
            if (apiResponse?.Data == null || apiResponse.Data.ToArray().Length == 0)
                throw new ArgumentException("There are no quotes by this movie or movie not found."); 
            return apiResponse.Data.ToArray();
        }
        catch (Exception e)
        {
            _logger.LogError(e.Message);
            throw;
        }
    }

    public async Task<IEnumerable<Quote>> GetByMovieAsync(Movie movie)
    {
        return await GetByMovieAsync(movie.Id);
    }
    
    
}