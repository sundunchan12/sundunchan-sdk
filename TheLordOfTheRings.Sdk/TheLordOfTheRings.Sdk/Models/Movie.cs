﻿using System.Text.Json.Serialization;

namespace TheLordOfTheRings.Sdk.Models;

public class Movie
{
    [JsonPropertyName("_id")]
    public string Id { get; set; }
    
    public string Name { get; set; }
    
    public int RuntimeInMinutes { get; set; }
    
    public int BudgetInMillions { get; set; }
    
    public double BoxOfficeRevenueInMillions { get; set; }
    
    public int AcademyRewardNominations { get; set; }
    
    public int AcademyAwardWins { get; set; }
    
    public double RottenTomatoesScore { get; set; }
}