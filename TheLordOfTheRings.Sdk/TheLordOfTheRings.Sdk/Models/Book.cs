﻿using System.Text.Json.Serialization;

namespace TheLordOfTheRings.Sdk.Models;

public class Book
{
    [JsonPropertyName("_id")]
    public string Id { get; set; }
    
    public string Name { get; set; }
}