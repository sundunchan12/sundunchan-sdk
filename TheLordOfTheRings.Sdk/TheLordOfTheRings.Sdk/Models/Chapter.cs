﻿using System.Text.Json.Serialization;

namespace TheLordOfTheRings.Sdk.Models;

public class Chapter
{
    [JsonPropertyName("_id")]
    public string Id { get; set; }
    
    [JsonPropertyName("chapterName")]
    public string Name { get; set; }
    
    [JsonPropertyName("book")]
    public string BookId { get; set; }
}