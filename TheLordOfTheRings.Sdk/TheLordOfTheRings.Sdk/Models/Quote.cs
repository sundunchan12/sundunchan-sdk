﻿using System.Text.Json.Serialization;

namespace TheLordOfTheRings.Sdk.Models;

public class Quote
{
    [JsonPropertyName("_id")]
    public string Id { get; set; }
    
    public string Dialog { get; set; }
    
    [JsonPropertyName("movie")]
    public string MovieId { get; set; }
    
    [JsonPropertyName("character")]
    public string CharacterId { get; set; }
}