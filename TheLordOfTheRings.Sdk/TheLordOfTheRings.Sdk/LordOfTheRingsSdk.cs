﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using TheLordOfTheRings.Contracts;
using TheLordOfTheRings.Sdk.Interfaces;
using TheLordOfTheRings.Sdk.Services;

namespace TheLordOfTheRings.Sdk;

public class LordOfTheRingsSdk
{
    public readonly IBookService Books;
    public readonly IChapterService Chapters;
    public readonly ICharacterService Characters;
    public readonly IMovieService Movies;
    public readonly IQuoteService Quotes;
    
    private readonly HttpClient _httpClient;
    private readonly ILoggerFactory _loggerFactory;
    public LordOfTheRingsSdk(string apiKey, bool useLogger = false)
    {
        _httpClient = new HttpClient
        {
            BaseAddress = new Uri(ApiRoutes.Base),
            DefaultRequestHeaders =
            {
                { "Authorization", $"Bearer {apiKey}" }
            },
        };

        if (useLogger) _loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
        else _loggerFactory = new NullLoggerFactory();
        
        Books = new BookService(_httpClient, _loggerFactory.CreateLogger<BookService>());
        Chapters = new ChapterService(_httpClient, _loggerFactory.CreateLogger<ChapterService>());
        Characters = new CharacterService(_httpClient, _loggerFactory.CreateLogger<CharacterService>());
        Movies = new MovieService(_httpClient, _loggerFactory.CreateLogger<MovieService>());
        Quotes = new QuoteService(_httpClient, _loggerFactory.CreateLogger<QuoteService>());

    }

    
    

}