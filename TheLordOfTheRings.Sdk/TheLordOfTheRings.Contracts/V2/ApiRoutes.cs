﻿namespace TheLordOfTheRings.Contracts;

public class ApiRoutes
{
    public const string Root = "https://the-one-api.dev";

    public const string Version = "v2";

    public const string Base = Root + "/" + Version;

    public static class Books
    {
        public const string GetAll = Base + "/book";

        public const string Get = Base + "/book/{id}";

        public const string GetBookChapters = Base + "/book/{id}/chapter";
    }

    public static class Movies
    {
        public const string GetAll = Base + "/movie";

        public const string Get = Base + "/movie/{id}";

        public const string GetMovieQuotes = Base + "/movie/{id}/quote";
    }

    public static class Characters
    {
        public const string GetAll = Base + "/character";

        public const string Get = Base + "/character/{id}";

        public const string GetCharacterQuotes = Base + "/character/{id}/quote";
    }
    
    public static class Quotes
    {
        public const string GetAll = Base + "/quote";

        public const string Get = Base + "/quote/{id}";
    }

    public static class Chapters
    {
        public const string GetAll = Base + "/chapter";

        public const string Get = Base + "/chapter/{id}";
    }
}