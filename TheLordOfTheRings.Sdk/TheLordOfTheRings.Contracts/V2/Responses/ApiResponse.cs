﻿using System.Text.Json.Serialization;

namespace TheLordOfTheRings.Contracts.Responses;

public class ApiResponse<TData>
{
    [JsonPropertyName("docs")]
    public IEnumerable<TData> Data { get; set; }
    
    public int Total { get; set; }
    
    public int Limit { get; set; }
    
    public int Offset { get; set; }
    
    public int Page { get; set; }
    
    public int Pages { get; set; }
}